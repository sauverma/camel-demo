Sample camel based components with below features:-
 
 1. Persistence layer with mysql via hibernate
 2. Integrations via message passing with activemq broker
 3. Dedicated bus for error logging per component
 4. Simple REST interface
 5. Integration with metrics (system/component & system metrics)
