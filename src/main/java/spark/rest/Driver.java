package spark.rest;

import org.apache.camel.component.amqp.AMQPComponent;
import org.apache.camel.impl.ExplicitCamelContextNameStrategy;
import org.apache.camel.main.Main;
import spark.rest.beans.OnboardingTask;
import spark.rest.kafkasvc.KafkaRouteBuilder;
import spark.rest.kafkasvc.KafkaService;
import spark.rest.onboardingsvc.OnboardingRouteBuilder;
import spark.rest.onboardingsvc.OnboardingService;
import spark.rest.onboardingsvc.producers.JsonConverter;

public class Driver extends Main {
    public static void main(String[] args) throws Exception {
        Main main = new Main();

        main.getOrCreateCamelContext().setNameStrategy(new ExplicitCamelContextNameStrategy("camel-demo"));

        AMQPComponent amqp = AMQPComponent.amqpComponent("amqp://localhost:32789", "admin", "admin");

        main.bind("amqp", amqp);

        main.bind("onboardSvc", new OnboardingService());
        main.addRouteBuilder(new OnboardingRouteBuilder());

        main.bind("kafkaSvc", new KafkaService());
        main.addRouteBuilder(new KafkaRouteBuilder());

        main.run();
    }
}
