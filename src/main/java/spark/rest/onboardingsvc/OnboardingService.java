package spark.rest.onboardingsvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.camel.Body;
import org.apache.camel.Headers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

public class OnboardingService {
    protected static final Logger LOG = LoggerFactory.getLogger(OnboardingService.class);
    ObjectMapper mapper = new ObjectMapper();

    public String ping() {
        return "pong";
    }

    public String saveOnboardingMeta(@Body String body) {
        LOG.info("saveOnboardingMeta: " + body);

        //throw new IllegalArgumentException("asdf");
        return body;
    }

    public void logMsg(String body) {
        LOG.info("logMsg: " + body);
    }
}
