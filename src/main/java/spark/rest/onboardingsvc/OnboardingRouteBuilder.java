package spark.rest.onboardingsvc;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.model.rest.RestBindingMode;
import spark.rest.beans.OnboardingTask;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
@ContextName("camel-demo")
public class OnboardingRouteBuilder extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        restConfiguration().component("spark-rest").apiContextPath("api-doc").port(18080)
                // and we enable json binding mode
                .bindingMode(RestBindingMode.json)
                // and output using pretty print
                .dataFormatProperty("prettyPrint", "true");

        rest("/onboarding")
                .get("/")
                .route().routeId("list onboardingmeta route")
                .toF("jpa:%s?consumeDelete=false&nativeQuery=select * from onboardingmeta", OnboardingTask.class.getName())
                .endRest();

        Processor processor = new Processor() {
            public void process(Exchange exchange) throws Exception {
                System.out.println("PRODUCER Received response: " + exchange.getIn().getBody(String.class));
            }
        };

        onException(Exception.class)
                .process(new Processor() {
                    public void process(Exchange exchange) throws Exception {
                        if (exchange.getProperty("CamelExceptionCaught") != null) {
                            exchange.getIn().setBody(exchange.getIn().getBody()
                                    + " : " + exchange.getProperty("CamelExceptionCaught"));
                        }
                    }
                })
                .to("log:GeneralError?level=ERROR")
                .to("amqp:queue:camel.onboard.svc.err.bus");

        from("amqp:queue:camel.onboard.svc.req.bus?subscriptionDurable=false&timeToLive=10000").routeId("onboarding route")
                .convertBodyTo(OnboardingTask.class)
                .toF("jpa:%s", OnboardingTask.class.getName())
                .inOut("amqp:queue:camel.kafka.svc.req.bus" +
                        "?receiveTimeout=30000" +
                        "&requestTimeout=30000" +
                        "&timeToLive=604800000")
                .process(processor);
    }
}
