package spark.rest.onboardingsvc.producers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.camel.*;
import spark.rest.beans.OnboardingTask;

import java.io.IOException;

@Converter
public class JsonConverter {
    private static ObjectMapper mapper = new ObjectMapper();

    @Converter
    public static <T> T convertGeneric(String jsonStr, Class<T> cls) throws IOException {
        return mapper.readValue(jsonStr, cls);
    }

    @Converter
    public static OnboardingTask convertSpecific(String jsonStr) throws IOException {
        return mapper.readValue(jsonStr, OnboardingTask.class);
    }
}
