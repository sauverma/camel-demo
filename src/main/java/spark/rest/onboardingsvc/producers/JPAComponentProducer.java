package spark.rest.onboardingsvc.producers;

import org.apache.camel.component.jpa.JpaComponent;
import org.springframework.transaction.PlatformTransactionManager;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.ws.rs.Produces;

public class JPAComponentProducer {
    @Produces
    @ApplicationScoped
    @Named("jpa")
    public JpaComponent jpaComponent(PlatformTransactionManager transactionManager, EntityManager entityManager) {
        JpaComponent component = new JpaComponent();
        component.setTransactionManager(transactionManager);
        component.setEntityManagerFactory(entityManager.getEntityManagerFactory());
        return component;
    }
}
