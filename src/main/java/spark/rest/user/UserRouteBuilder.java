package spark.rest.user;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;

/**
 * Define REST services using the Camel REST DSL
 */
public class UserRouteBuilder extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        // configure we want to use spark-rest on port 8080 as the component for the rest DSL
        // and for the swagger api-doc as well
        restConfiguration().component("spark-rest").apiContextPath("api-doc").port(8080)
                // and we enable json binding mode
                .bindingMode(RestBindingMode.json)
                // and output using pretty print
                .dataFormatProperty("prettyPrint", "true");

        // this user REST service is json only
        rest("/user").consumes("application/json").produces("application/json")

                .get("/view/{id}").outType(User.class)
                .to("bean:userService?method=getUser(${header.id})")

                .get("/list").outTypeList(User.class)
                .to("bean:userService?method=listUsers")

                .put("/update").type(User.class).outType(User.class)
                .to("bean:userService?method=updateUser");

        from("direct:in")
                .setHeader("test", constant("new.name"))
                .to("metrics:counter:name.not.used")
                .to("direct:out");
    }

}
