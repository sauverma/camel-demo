package spark.rest.kafkasvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KafkaService {
    protected static final Logger LOG = LoggerFactory.getLogger(KafkaService.class);
    ObjectMapper mapper = new ObjectMapper();

    public String ping() {
        return "pong";
    }

    public String logMsg(String body) {
        LOG.info("logMsg: " + body);
        return body;
    }
}
