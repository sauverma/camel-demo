package spark.rest.kafkasvc;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;

public class KafkaRouteBuilder extends RouteBuilder {
    @Override
    public void configure() throws Exception {
//        restConfiguration().component("spark-rest").apiContextPath("api-doc").port(8080)
//                // and we enable json binding mode
//                .bindingMode(RestBindingMode.json)
//                // and output using pretty print
//                .dataFormatProperty("prettyPrint", "true");

        Processor processor = new Processor() {
            public void process(Exchange exchange) throws Exception {
                System.out.println("CONSUMER received message: " + exchange.getIn().getBody(String.class));

                exchange.getIn().setBody("I Saw it!!! It contained: " + exchange.getIn().getBody(String.class));
            }
        };

        from("amqp:queue:camel.kafka.svc.req.bus")
                .bean(KafkaService.class, "logMsg")
                .process(processor);
    }
}
