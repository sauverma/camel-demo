package spark.rest.beans;

public class KafkaMeta {
    String topic;
    int start;
    int end;

    public KafkaMeta(String topic, int start, int end) {
        this.topic = topic;
        this.start = start;
        this.end = end;
    }
}
