package spark.rest.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity(name = "onboardingmeta")
public class OnboardingTask implements Serializable {
    @Id
    @GeneratedValue
    private Integer evtId;

    Integer dpid;
    String dpName;
    String dpType;
    String basePath;
    String taskStatus = "SUBMITTED";

    public OnboardingTask() {}

    public int getEvtId() {
        return evtId;
    }

    public void setEvtId(int evtId) {
        this.evtId = evtId;
    }

    public Integer getDpid() {
        return dpid;
    }

    public void setDpid(Integer dpid) {
        this.dpid = dpid;
    }

    public String getDpName() {
        return dpName;
    }

    public void setDpName(String dpName) {
        this.dpName = dpName;
    }

    public String getDpType() {
        return dpType;
    }

    public void setDpType(String dpType) {
        this.dpType = dpType;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    @Override
    public String toString() {
        return "OnboardingTask{" +
                "evtId=" + evtId +
                ", dpid=" + dpid +
                ", dpName='" + dpName + '\'' +
                ", dpType='" + dpType + '\'' +
                ", basePath='" + basePath + '\'' +
                ", taskStatus='" + taskStatus + '\'' +
                '}';
    }
}
