import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Expression;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class FileCopierWithCamel {
    public static void main(String args[]) throws Exception {
        CamelContext context = new DefaultCamelContext();

        context.addRoutes(new RouteBuilder() {
            public void configure() {
                from("direct:in")
                        .log("Processing: ${id}")
                        .to("direct:processMsg")
                        .log("Message: ${id}:${body}");

                from("direct:processMsg")
                        .setBody(constant("hello, world!!!"));

            }
        });

        ProducerTemplate template = context.createProducerTemplate();
        context.start();

        template.sendBody("direct:in", "hey there");

        Thread.sleep(1000);
        context.stop();
    }
}
